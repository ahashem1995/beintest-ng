import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import * as SLUGS from './config/slugs';
import {PageNotFoundComponent} from './shared/components/page-not-found/page-not-found.component';
import {UserListingComponent} from './shared/components/user-listing/user-listing.component';
import {CanLoadExpertsGuardService} from './shared/guards/can-load-experts-guard.service';

const routes: Routes = [
    {
        path: SLUGS.EXPERTS,
        loadChildren: () => import('./modules/experts/experts.module').then(m => m.ExpertsModule),
        canLoad: [CanLoadExpertsGuardService]
    },
    {
        path: SLUGS.USERS,
        component: UserListingComponent
    },
    {
        path: SLUGS.PAGE_NOT_FOUND,
        component: PageNotFoundComponent
    },
    {
        path: '',
        redirectTo: SLUGS.USERS,
        pathMatch: 'full'
    },
    {
        path: '**',
        redirectTo: SLUGS.PAGE_NOT_FOUND
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
