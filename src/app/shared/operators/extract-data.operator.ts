import { Observable } from 'rxjs';
import {ServerResponse} from '../models/dto/server-response.model';

export function extractData<T>(source: Observable<ServerResponse<T>>): Observable<T> {
  return new Observable(observer => {
    source.subscribe({
      next(value) {
        observer.next(value.data);
      },
      error(err: any) {
        observer.error(err);
      },
      complete() {
        observer.complete();
      }
    });
  });
}
