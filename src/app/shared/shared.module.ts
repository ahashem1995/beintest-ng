import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';
import { ExpertItemComponent } from '../modules/experts/components/expert-item/expert-item.component';
import {RouterModule} from '@angular/router';
import { UserListingComponent } from './components/user-listing/user-listing.component';
import {MatGridListModule, MatProgressSpinnerModule} from '@angular/material';
import {ExpertsModule} from '../modules/experts/experts.module';
import { UserItemComponent } from './components/user-item/user-item.component';


@NgModule({
  declarations: [PageNotFoundComponent, UserListingComponent, UserItemComponent],
    imports: [
        CommonModule,
        RouterModule,
        MatProgressSpinnerModule,
        MatGridListModule,
        ExpertsModule
    ],
  exports: [
    PageNotFoundComponent
  ]
})
export class SharedModule {
}
