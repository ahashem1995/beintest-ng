import {Injectable} from '@angular/core';
import {AbstractControl, FormArray, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {CustomValidators} from 'ng2-validation';

type Validator =
  string
  | 'required'
  | 'requiredTrue'
  | 'digits'
  | 'mobileNumber'
  | 'email'
  | ((control: AbstractControl) => { [key: string]: boolean } | null);

export class ControlConfig {
  defaultValue?: any;
  disabled?: boolean;
  validators?: Validator[];

  constructor(defaultValue: any = null, validators: Validator[] = [], disabled: boolean = false) {
    this.defaultValue = defaultValue;
    this.validators = validators;
    this.disabled = disabled;
  }
}

export class ArrayConfig {
  constructor(public defaultValue: (ControlConfig | FormConfig)[] = []) {
  }
}

export class FormConfig {
  [key: string]: ControlConfig | FormConfig;
}

@Injectable({
  providedIn: 'root'
})
export class BeInFormBuilderService {

  constructor() {
  }

  /**
   * Build ValidatorFn array out of our string typed validators
   * @param validators Validator[]
   * @param form FormGroup to get other controls from
   */
  public static buildValidators(validators: Validator[], form: BeInFormGroup = null): ValidatorFn {
    const validatorList = [];
    for (const validator of validators) {
      if (typeof validator === 'string') {
        if (validator === 'required') {
          validatorList.push(Validators.required);
        }
        if (validator === 'requiredTrue') {
          validatorList.push(Validators.requiredTrue);
        }
        if (validator === 'email') {
          validatorList.push(CustomValidators.email);
        }
        if (validator === 'digits') {
          validatorList.push(CustomValidators.digits);
        }
        if (validator === 'mobileNumber') {
          validatorList.push(Validators.pattern('[0-9]{4,14}'));
        }
        if (validator.includes('minLength')) {
          validatorList.push(Validators.minLength(+validator.split(':')[1]));
        }
        if (validator.includes('maxLength')) {
          validatorList.push(Validators.maxLength(+validator.split(':')[1]));
        }
        if (validator.includes('min:')) {
          validatorList.push(Validators.min(+validator.split(':')[1]));
        }
        if (validator.includes('max:')) {
          validatorList.push(Validators.max(+validator.split(':')[1]));
        }
        if (validator.includes('pattern:')) {
          validatorList.push(Validators.pattern(validator.split('pattern:')[1]));
        }
        if (validator.includes('notEqual')) {
          validatorList.push(CustomValidators.notEqual(validator.split('notEqual:')[1]));
        }
        if (validator.includes('equals')) {
          validatorList.push(CustomValidators.equalTo(form.get(validator.split('equals:')[1])));
        }
      } else if (typeof validator === 'function') {
        validatorList.push(validator);
      }
    }
    return Validators.compose(validatorList);
  }

  /**
   * Make a new beInMedia form group
   * @param form controls map
   * @param validators to be applied on form level
   * @returns BeInFormGroup
   */
  private static group(form: {}, validators: any[] = null): BeInFormGroup {
    return new BeInFormGroup(form, validators);
  }

  /**
   * Bind form initial values
   * @param formConfig FormConfig
   * @param validators Validator[]
   * @returns BeInFormGroup
   */
  bindForm(formConfig: FormConfig, validators: Validator[] = null): BeInFormGroup {
    const form = {};
    const keys = Object.keys(formConfig);
    let fieldConfig: ControlConfig | FormConfig | ArrayConfig;
    for (const key of keys) {
      fieldConfig = formConfig[key];
      if (fieldConfig instanceof ControlConfig) {
        form[key] = ControlFactory.build(fieldConfig);
      } else if (fieldConfig instanceof ArrayConfig) {
        form[key] = ArrayFactory.build(fieldConfig);
      } else {
        form[key] = this.bindForm(fieldConfig);
      }
    }

    return BeInFormBuilderService.group(form, validators);
  }
}

export class ControlFactory {
  static build(fieldConfig: ControlConfig) {
    return new FormControl(
      fieldConfig ? fieldConfig.defaultValue : null,
      BeInFormBuilderService.buildValidators(fieldConfig && fieldConfig.validators ? fieldConfig.validators : [])
    );
  }
}

export class ArrayFactory {
  static build(fieldConfig: ArrayConfig) {
    const controls = fieldConfig.defaultValue.map(c => ControlFactory.build(c));
    return new FormArray(controls);
  }
}

export class BeInFormGroup extends FormGroup {

  /**
   * Sets a form to submitted state on ngSubmit
   */
  private _isSubmitted = false;

  constructor(form: {}, validators = null, isSubmitted: boolean = false) {
    super(form, validators);
    this._isSubmitted = isSubmitted;
  }

  /**
   * Reset form values with provided ones
   * @param value any
   * @param options {onlySelf: boolean, emitEvent: boolean}
   */
  reset(value?: any, options?: { onlySelf?: boolean; emitEvent?: boolean }): void {
    super.reset(value, options);
    this.isSubmitted = false;
  }

  set isSubmitted(isSubmitted: boolean) {
    this._isSubmitted = isSubmitted;
  }

  get isSubmitted(): boolean {
    return this._isSubmitted;
  }
}
