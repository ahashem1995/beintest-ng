import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalsService {
  durations: Array<{ value: number, label: string }> = [
    {value: 15, label: '15 min'},
    {value: 30, label: '30 min'},
    {value: 45, label: '45 min'},
    {value: 60, label: '1 hour'},
  ];
}
