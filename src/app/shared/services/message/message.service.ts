import {messages} from '../../utils/messages';

export class MessageService {
    static get(key: string) {
        return messages[key] || key;
    }
}
