import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../models/dto/response/user.model';
import {API} from '../../config/api';
import {ServerResponse} from '../models/dto/server-response.model';
import {extractData} from '../operators/extract-data.operator';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    user: User = null;

    constructor(private http: HttpClient) {
    }

    getAll(): Observable<Array<User>> {
        const url = `${API.users}`;
        return this.http.get<ServerResponse<Array<User>>>(url)
            .pipe(extractData);
    }
}
