export const messages = {
    required_field: 'Required field',
    max_length: ':field: must be at most :n: characters length.',
    min_length: ':field: must be at least :n: characters length.',
    max: ':field: must be at most :n:.',
    min: ':field: must be at least :n:.',
    digits: 'Only digits are allowed',
    invalid_email: 'Please enter a valid email address',
    email_registered: 'The email is already registered'
};
