import {Injectable} from '@angular/core';
import {CanLoad, Route, Router, UrlSegment} from '@angular/router';
import {Observable} from 'rxjs';
import {UserService} from '../services/user.service';

@Injectable({
    providedIn: 'root'
})
export class CanLoadExpertsGuardService implements CanLoad {

    constructor(private router: Router, private user: UserService) {
    }

    canLoad(route: Route, segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
        const canLoad = !!this.user.user && !!this.user.user.timezone;
        if (!canLoad) {
            this.router.navigate(['/users']).then();
        }
        return canLoad;
    }
}
