import { TestBed } from '@angular/core/testing';

import { CanLoadExpertsGuardService } from './can-load-experts-guard.service';

describe('CanLoadExpertsGuardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CanLoadExpertsGuardService = TestBed.get(CanLoadExpertsGuardService);
    expect(service).toBeTruthy();
  });
});
