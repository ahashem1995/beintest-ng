import { AbstractControl } from '@angular/forms';
import {BeInFormGroup} from '../services/be-in-form-builder.service';
import {BeInFormComponent} from '../components/be-in-form/be-in-form.component';

export const hasRequiredValidator = (abstractControl: AbstractControl): boolean => {
    if (abstractControl.validator) {
        const validator = abstractControl.validator({} as AbstractControl);
        if (validator && validator.required) {
            return true;
        }
    }
    if (abstractControl['controls']) {
        for (const controlName in abstractControl['controls']) {
            if (abstractControl['controls'][controlName]) {
                if (hasRequiredValidator(abstractControl['controls'][controlName])) {
                    return true;
                }
            }
        }
    }
    return false;
};


export function isFormSubmitted(form: BeInFormGroup, formComponent: BeInFormComponent) {
    if (form) {
        return form.isSubmitted;
    }
    if (!formComponent || !formComponent.htmlForm || !formComponent.htmlForm.nativeElement) {
        return false;
    }
    return formComponent.htmlForm.nativeElement.classList.contains('ng-submitted');
}
