export class ServerResponse<T> {
  data: T;
  message?: string;
  errors?: any;

  constructor(data?: T, message?: string, errors?: any) {
    this.data = data;
    this.message = message;
    this.errors = errors;
  }
}
