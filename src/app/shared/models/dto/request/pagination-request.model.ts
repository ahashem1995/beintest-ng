export class PaginationRequest {

  constructor(public page: number = 1,
              public sort: string = 'id',
              public direction: 'asc' | 'desc' | '' = 'desc',
              public perPage: number = 25,
              public query: string = '') {
    this.sort = sort;
    this.direction = direction;
    this.page = page;
    this.perPage = perPage;
    this.query = query;
  }

  queryParams(): string {
    let queryParamsString = `page=${this.page}&page_size=${this.perPage}`;
    queryParamsString = queryParamsString + (this.sort ? `&order_by=${this.sort}` : '');
    queryParamsString = queryParamsString + (this.direction ? `&order_dir=${this.direction}` : '');
    queryParamsString = queryParamsString + (this.query ? `&query=${this.query}` : '');
    return queryParamsString;
  }
}
