class PaginationMeta {
  current_page: number;
  from: number;
  to: number;
  last_page: number;
  path: string;
  per_page: number;
  total: number;
}

class PaginationLinks {
  first: string;
  last: string;
  prev: string;
  next: string;
}

export class PaginationResult<T> {
  data: T[];
  meta: PaginationMeta;
  links: PaginationLinks;

  constructor(data: T[], meta?: PaginationMeta, links?: PaginationLinks) {
    this.data = data;
    this.meta = meta;
    this.links = links;
  }
}
