import { ChangeDetectorRef, Injector, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {BaseUIState} from './base-ui-state.model';
import {AbstractBaseNgComponent, IBaseNg} from './base-ng.interface';

export class BaseNgComponent<T extends BaseUIState = BaseUIState> extends AbstractBaseNgComponent
  implements IBaseNg, OnInit, OnDestroy {

  constructor(protected injector?: Injector, private childState?: {}) {
    super();
    this.route = this.injector.get(ActivatedRoute);
    this.router = this.injector.get(Router);
    this.renderer2 = this.injector.get(Renderer2);
    this.cdRef = this.injector.get(ChangeDetectorRef);
    // Placeholder..
    if (this.childState) {
      //  console.log('Component state', this.childState);
    }
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}

