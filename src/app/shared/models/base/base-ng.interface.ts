import { ActivatedRoute, Router } from '@angular/router';
import { ChangeDetectorRef, Renderer2 } from '@angular/core';
import { Subscription } from 'rxjs';
import { DatePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import {BaseUIState} from './base-ui-state.model';

export interface IBaseNg<T extends BaseUIState = BaseUIState> {
    /**
     * To access url, get & fragment params
     */
    route: ActivatedRoute;

    /**
     * Angular router utility
     */
    router: Router;

    /**
     * Document handler
     */
    renderer2: Renderer2;

    /**
     * Change detection reference
     */
    cdRef: ChangeDetectorRef;

    /**
     * Angular date pipe
     */
    datePipe: DatePipe;

    /**
     * Math object accessor
     */
    math: Math;

    /**
     * Ui state
     */
    state: T;

    /**
     * To append tears on
     */
    subscription: Subscription;
}

export abstract class AbstractBaseNgComponent<T extends BaseUIState = BaseUIState> implements IBaseNg {
    /**
     * To access url, get & fragment params
     */
    route: ActivatedRoute;

    /**
     * Angular router utility
     */
    router: Router;

    /**
     * Document handler
     */
    renderer2: Renderer2;

    /**
     * Http client to fetch data
     */
    http: HttpClient;

    /**
     * Change detection reference
     */
    cdRef: ChangeDetectorRef;

    /**
     * Angular date pipe
     */
    datePipe: DatePipe;

    /**
     * Math object accessor
     */
    math: Math;

    /**
     * Ui state
     */
    state: T;

    /**
     * To append tears on
     */
    subscription: Subscription;
}
