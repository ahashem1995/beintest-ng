import {BaseNgComponent} from './base-ng.component';
import {BaseUIState} from './base-ui-state.model';
import {Injector} from '@angular/core';
import {ExpertService} from '../../../modules/experts/services/expert.service';
import {GlobalsService} from '../../services/globals.service';
import {BeInFormBuilderService} from '../../services/be-in-form-builder.service';
import * as moment from 'moment';
import {UserService} from '../../services/user.service';

export class BaseAppComponent<T extends BaseUIState = BaseUIState> extends BaseNgComponent<T> {

    protected expert: ExpertService;

    public user: UserService;

    public globals: GlobalsService;

    public fb: BeInFormBuilderService;

    protected moment = moment;

    constructor(injector?: Injector, childState?: {}) {
        super(injector, childState);
        this.expert = this.injector.get(ExpertService);
        this.user = this.injector.get(UserService);
        this.globals = this.injector.get(GlobalsService);
        this.fb = this.injector.get(BeInFormBuilderService);
    }
}
