import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormControl, FormGroupDirective, FormsModule, NgForm, ReactiveFormsModule} from '@angular/forms';
import {BeInFormComponent} from './be-in-form.component';
import {AbstractFieldComponent} from '../abstract-field/abstract-field.component';
import {ErrorStateMatcher} from '@angular/material';
import {BeInFormGroup} from '../../services/be-in-form-builder.service';

export class SubmissionErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const beInFormGroup = control ? (control.parent as BeInFormGroup) : null;
    return !!(beInFormGroup && control && control.invalid && beInFormGroup.isSubmitted);
  }
}

@NgModule({
  declarations: [
    BeInFormComponent,
    AbstractFieldComponent
  ],
  exports: [
    BeInFormComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [{ provide: ErrorStateMatcher, useClass: SubmissionErrorStateMatcher }]
})
export class BeInFormModule {
}
