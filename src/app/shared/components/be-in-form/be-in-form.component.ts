import {
    AfterViewInit,
    Component,
    ContentChildren,
    ElementRef,
    EventEmitter,
    Input,
    Output,
    QueryList,
    ViewChild
} from '@angular/core';
import {NgForm, NgModel} from '@angular/forms';
import {BeInFormGroup} from '../../services/be-in-form-builder.service';

const inputTypes = ['input', 'select', 'checkbox', 'radio', 'app-input-select'];

@Component({
    selector: 'be-in-form',
    template: `
        <!-- Reactive form -->
        <form *ngIf="formGroup" #htmlForm class="{{cssClass}}" [formGroup]="formGroup" (ngSubmit)="submit()">
            <ng-container *ngTemplateOutlet="ngContentOutlet"></ng-container>
        </form>
        <!-- Template driven form -->
        <form *ngIf="!formGroup" #htmlForm #ngForm="ngForm" class="{{cssClass}}" (ngSubmit)="submit(ngForm.value)">
            <ng-container *ngTemplateOutlet="ngContentOutlet"></ng-container>
        </form>
        <!-- Only one ng-content should exists -->
        <ng-template #ngContentOutlet>
            <ng-content></ng-content>
        </ng-template>
    `,
})
export class BeInFormComponent implements AfterViewInit {

    /**
     * Form values container in case of reactive form
     */
    @Input() formGroup: BeInFormGroup = null;

    /**
     * Inner form css classes
     */
    @Input() cssClass = '';

    /**
     * Html form element reference
     */
    @ViewChild('htmlForm', {static: false}) htmlForm: ElementRef<HTMLFormElement>;

    /**
     * Ng template driven form
     */
    @ViewChild(NgForm, {static: false}) ngForm: NgForm;

    /**
     * Ng models inputs in case of template form
     */
    @ContentChildren(NgModel) public ngModels: QueryList<NgModel>;

    /**
     * Event fired on form submit to notify parent or send form value
     */
    @Output('onSubmit') onSubmit: EventEmitter<any> = new EventEmitter<any>();

    constructor() {
    }

    ngAfterViewInit(): void {
        this.fillInTemplateForm();
    }

    /**
     * Fill in template form with ng models as they live in ng-content
     */
    private fillInTemplateForm(): void {
        if (!this.formGroup) { // In case of template driven forms
            // To populate form with ng models
            const ngContentModels = this.ngModels.toArray();
            ngContentModels.forEach(m => this.ngForm.addControl(m));
        }
    }

    /**
     * Form submit event handler
     * Modifies isSubmitted form group property to true
     */
    submit(formValue?: any, emit: boolean = true): void {
        this.appendSubmittedClass();
        // If reactive form
        if (this.formGroup) {
            this.formGroup.isSubmitted = true;
        }
        if (emit) {
            this.onSubmit.next(formValue);
        }
    }

    reset(value?: any, options?: { onlySelf?: boolean; emitEvent?: boolean }): void {
        this.removeSubmittedClass();
        this.formGroup.reset(value, options);
    }

    /**
     * Append ng-submitted class to all children fields
     */
    private appendSubmittedClass(): void {
        const collections: Array<HTMLCollectionOf<Element>> = [];
        const formElem = this.htmlForm.nativeElement;
        formElem.classList.add('ng-submitted');
        let field;
        inputTypes.forEach(i => collections.push(formElem.getElementsByTagName(i)));
        collections.forEach(c => {
            for (let i = 0; i < c.length; i++) {
                field = c.item(i);
                if (field && field.classList) {
                    field.classList.add('ng-submitted');
                }
            }
        });
    }

    /**
     * Remove ng-submitted class to all children fields
     */
    private removeSubmittedClass(): void {
        const collections: Array<HTMLCollectionOf<Element>> = [];
        const formElem = this.htmlForm.nativeElement;
        formElem.classList.remove('ng-submitted');
        let field;
        inputTypes.forEach(i => collections.push(formElem.getElementsByTagName(i)));
        collections.forEach(c => {
            for (let i = 0; i < c.length; i++) {
                field = c.item(i);
                if (field && field.classList) {
                    field.classList.remove('ng-submitted');
                }
            }
        });
    }

}
