import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputSelectComponent } from './input-select.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FieldErrorsModule } from '../field-errors/field-errors.module';
import { MatInputModule, MatOptionModule, MatSelectModule } from '@angular/material';

@NgModule({
    declarations: [
        InputSelectComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        FieldErrorsModule,
        MatInputModule,
        MatOptionModule,
        MatSelectModule
    ],
    exports: [
        InputSelectComponent
    ]
})
export class InputSelectModule {
}
