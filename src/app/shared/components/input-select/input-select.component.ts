import {Component, forwardRef, Injector, Input} from '@angular/core';
import {AbstractFieldComponent} from '../abstract-field/abstract-field.component';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
  selector: 'app-input-select',
  templateUrl: './input-select.component.html',
  styleUrls: ['./input-select.component.scss'],
  providers: [
    {provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => InputSelectComponent), multi: true}
  ],
})
export class InputSelectComponent extends AbstractFieldComponent implements ControlValueAccessor {

  /**
   * Select options
   */
  @Input() options: Array<any> = [];

  /**
   * First option
   */
  @Input() showEmptyOption = true;

  /**
   *
   */
  @Input() labelKey = '';

  /**
   *
   */
  @Input() valueKey = '';

  constructor(injector: Injector) {
    super(injector);
  }

  getValue(option: any): any {
    return this.valueKey ? (this.valueKey.split('.').reduce((prev, current) => prev ? prev[current] : option, option)) : option;
  }
}
