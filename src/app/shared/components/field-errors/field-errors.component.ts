import { Component, forwardRef, Inject, Injector, Input, Optional, ViewEncapsulation } from '@angular/core';
import { FormControl, NgModel } from '@angular/forms';
import {BeInFormGroup} from '../../services/be-in-form-builder.service';
import {BaseNgComponent} from '../../models/base/base-ng.component';
import {MessageService} from '../../services/message/message.service';
import {isFormSubmitted} from '../../helpers/form.functions';
import {BeInFormComponent} from '../be-in-form/be-in-form.component';

enum ErrorType {
    REQUIRED = 'required',
    MAX_LENGTH = 'maxlength',
    MIN_LENGTH = 'minlength',
    MAX = 'max',
    MIN = 'min',
    EMAIL = 'email',
    DIGITS = 'digits',
    EMAIL_REGISTERED = 'emailRegistered',
    SERVER_ERROR = 'serverError'
}

const errorMessageMap = {
    required: 'required_field',
    maxlength: 'max_length',
    minlength: 'min_length',
    max: 'max',
    min: 'min',
    digits: 'digits',
    email: 'invalid_email',
    emailRegistered: 'email_registered',
    serverError: 'serverError'
};

@Component({
    selector: '[be-in-field-errors]',
    templateUrl: './field-errors.component.html',
    styleUrls: ['./field-errors.component.scss'],
    encapsulation: ViewEncapsulation.ShadowDom
})
export class FieldErrorsComponent extends BaseNgComponent {

    /**
     * Form values container
     */
    @Input() form: BeInFormGroup;

    /**
     * Form control name
     */
    @Input() private _control: string;

    /**
     * To display error
     */
    @Input() label: string;

    /**
     * Form control
     */
    @Input() formControl: FormControl;

    /**
     * Ng model support
     */
    @Input() model: NgModel;

    /**
     * Should this component checks for form submission
     */
    @Input() submissionCheck = true;

    /**
     * Should this component checks for form dirty
     */
    @Input() dirtyCheck = false;

    /**
     * Should this component checks for form touched
     */
    @Input() touchedCheck = false;

    /**
     * Enum reference
     */
    error = ErrorType;

    constructor(injector: Injector, @Optional() @Inject(forwardRef(() => BeInFormComponent)) private _form: BeInFormComponent) {
        super(injector);
    }

    /**
     * Returns error string related to error type
     * @param error ErrorType
     * @param replacement array to apply arguments on message variables
     */
    msg(error: ErrorType, replacement: { replace: string, with: any }[] = []): string {
        let message = MessageService.get(errorMessageMap[error]);
        replacement.forEach(r => message = message.replace(r.replace, r.with));
        return message;
    }

    /**
     * Returns the error inside the form control
     * @param err ErrorType
     * @param key string
     */
    getError(err: ErrorType, key: string = null): any {
        return key ? this.modelControl.getError(err)[key] : this.modelControl.getError(err);
    }

    /**
     * Checks if form control or ng model a specific error
     * @param err ErrorType
     */
    hasError(err: ErrorType): boolean {
        return this.modelControl.hasError(err);
    }

    /**
     * Form control accessor
     * @returns FormControl
     */
    private get modelControl(): FormControl {
        if (!this.form) {
            if (!this.model) {
                return null;
            }
            return this.model.control;
        }
        return this.formControl;
    }

    /**
     * Returns if parent form is submitted
     */
    private get isSubmitted(): boolean {
        return isFormSubmitted(this.form, this._form);
    }

    /**
     * Show errors for current field
     */
    get showErrors(): boolean {
        return this.modelControl && this.modelControl.invalid &&
            ((this.dirtyCheck && this.modelControl.dirty) ||
                (this.touchedCheck && this.modelControl.touched) ||
                (this.submissionCheck && this.isSubmitted));
    }

    @Input()
    set control(control: string) {
        this._control = control;
        this.formControl = (this._control && this.form) ? this.form.get(this._control) as FormControl : null;
    }

    get control(): string {
        return this._control;
    }

}
