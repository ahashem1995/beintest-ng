import { NgModule } from '@angular/core';
import { FieldErrorsComponent } from './field-errors.component';
import { CommonModule } from '@angular/common';
import { MatInputModule } from '@angular/material';

@NgModule({
    imports: [
        CommonModule,
        MatInputModule
    ],
    declarations: [
        FieldErrorsComponent
    ],
    exports: [
        FieldErrorsComponent
    ]
})
export class FieldErrorsModule { }
