import {Component, Injector, Input, OnInit} from '@angular/core';
import {ExpertHeader} from '../../../modules/experts/models/dto/response/expert.model';
import {User} from '../../models/dto/response/user.model';
import {BaseAppComponent} from '../../models/base/base-app.component';

@Component({
    selector: 'app-user-item',
    templateUrl: './user-item.component.html',
    styleUrls: ['./user-item.component.scss']
})
export class UserItemComponent extends BaseAppComponent implements OnInit {

    @Input() dataSource: User;

    constructor(injector: Injector) {
        super(injector);
    }

    ngOnInit() {
    }

    setUser(): void {
        this.user.user = this.dataSource;
    }

}
