import {Component, Injector, OnInit} from '@angular/core';
import {BaseAppComponent} from '../../models/base/base-app.component';
import {BaseUIState} from '../../models/base/base-ui-state.model';
import {PaginationRequest} from '../../models/dto/request/pagination-request.model';
import {finalize} from 'rxjs/operators';
import {PaginationResult} from '../../models/dto/response/pagination-result.model';
import {ExpertHeader} from '../../../modules/experts/models/dto/response/expert.model';
import {User} from '../../models/dto/response/user.model';

@Component({
    selector: 'app-user-listing',
    templateUrl: './user-listing.component.html',
    styleUrls: ['./user-listing.component.scss']
})
export class UserListingComponent extends BaseAppComponent implements OnInit {

    users: Array<User> = [];

    constructor(injector: Injector) {
        super(injector);
        this.state = new BaseUIState();
    }

    ngOnInit() {
        this.getData();
    }

    private getData() {
        this.state.setIsLoading();
        this.user.getAll()
            .pipe(finalize(() => this.state.setIsLoading(false)))
            .subscribe(response => this.users = response);
    }

}
