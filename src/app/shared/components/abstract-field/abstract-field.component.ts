import { AfterViewInit, Component, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, NgModel } from '@angular/forms';
import { Subscription } from 'rxjs';
import {BaseUIState} from '../../models/base/base-ui-state.model';
import {BaseNgComponent} from '../../models/base/base-ng.component';
import {BeInFormGroup} from '../../services/be-in-form-builder.service';

@Component({ template: '' })
export class AbstractFieldComponent<T extends BaseUIState = BaseUIState> extends BaseNgComponent implements OnInit, AfterViewInit {

    /**
     * Base url for all new icons
     */
    protected static iconsBaseUrl = 'assets/vector-design-theme/images/new_icons';

    /**
     * Field id for development purposes
     */
    @Input() id: string = null;

    /**
     * Field name to support ng model
     */
    @Input() name: string;

    /**
     * Field type
     */
    @Input() type = 'text';

    /**
     * Field label to display
     */
    @Input() label = '';

    /**
     * Hint to display under the input
     */
    @Input() hint = '';

    /**
     * Select space placeholder
     */
    @Input() placeholder: string = null;

    /**
     * Additional css classes to be appended to field
     */
    @Input() cssClass: string;

    /**
     * Auto complete value
     */
    @Input() autoComplete = null;

    /**
     * Parent values form group container
     */
    @Input() form: BeInFormGroup;

    /**
     * Is field required
     */
    @Input() required = false;

    /**
     * Is field disabled
     */
    @Input() disabled: boolean;

    /**
     * Is field readonly
     */
    @Input() readOnly: boolean;

    /**
     * Should we show asterisk when field is required
     */
    @Input() showAsterisk = true;

    /**
     * Should we show label
     */
    @Input() showLabel = true;

    /**
     * Show field errors
     */
    @Input() showErrors = true;

    /**
     * Should this component checks for form submission
     */
    @Input() submissionCheck = true;

    /**
     * Should this component checks for form dirty
     */
    @Input() dirtyCheck = false;

    /**
     * Should this component checks for form touched
     */
    @Input() touchedCheck = false;

    /**
     * Control name
     */
    protected _control: string;

    /**
     * Form control
     */
    protected _formControl: FormControl = null;

    /**
     * Element reference
     */
    @ViewChild('fieldElement', {static: false}) fieldElement: NgModel;

    /**
     * Field default value
     */
    protected _value: any;

    /**
     * Icon name as an image name with its extention
     */
    private _icon = '';

    /**
     * Change propagator
     * @param _ any
     */
    propagateChange = (_: any) => { };

    /**
     * Touch propagator
     * @param _ any
     */
    propagateTouch = (_: any) => { };

    constructor(injector: Injector) {
        super(injector);
    }

    ngOnInit(): void {
        this.subscription = new Subscription();
        if (this.formControl) {
            this.subscription.add(
                this.formControl.valueChanges.subscribe(value => {
                    // console.log('this.value', value);
                    this._value = value;
                })
            );
        }
    }

    ngAfterViewInit(): void {
        this.cdRef.detectChanges();
    }

    writeValue(value: any): void {
        if (value) {
            this._value = value;
        }
    }

    registerOnChange(fn: any): void {
        this.propagateChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.propagateTouch = fn;
    }

    setDisabledState(isDisabled: boolean): void {
    }

    @Input()
    set control(control: string) {
        this._control = control;
        if (!this._formControl && this.form) {
            this.formControl = this.form.get(this._control) as FormControl;
        }
    }

    get control(): string {
        return this._control;
    }

    @Input()
    set value(val) {
        this._value = val;
        // Update form control in case of dealing with [form] & [control]
        // Dealing with [formControl] will be updated automatically
        if (this._formControl) {
            this._formControl.setValue(this._value);
        }
        this.propagateChange(this._value);
    }

    get value(): any {
        return this._value;
    }

    @Input()
    set formControl(formCtrl) {
        this._formControl = formCtrl;
        if (this._formControl) {
            if (!this.form) {
                this.form = this._formControl.parent as BeInFormGroup;
            }
            this.value = this._formControl.value;
        }
    }

    get formControl(): FormControl {
        return this._formControl;
    }

    @Input()
    set icon(iconName: string) {
        this._icon = (iconName && iconName.includes(AbstractFieldComponent.iconsBaseUrl))
          ? iconName
          : `${AbstractFieldComponent.iconsBaseUrl}/${iconName}`;
        if (iconName === null) {
            this._icon = null;
        }
    }

    get icon(): string {
        return this._icon;
    }

    /**
     * Display field errors only if showErrors flag is true & there is a fieldElement (NgModel) or formControl
     */
    get displayErrors(): boolean {
        return this.showErrors && ((!!this.formControl && !!this.form) || !!this.fieldElement);
    }

    /**
     * Form control accessor
     * @returns FormControl
     */
    protected get modelControl(): FormControl {
        if (!this.form) {
            if (!this.fieldElement) {
                return null;
            }
            return this.fieldElement.control;
        }
        return this.formControl;
    }
}
