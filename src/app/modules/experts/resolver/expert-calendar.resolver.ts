import {ExpertService} from '../services/expert.service';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {ExpertCalendar} from '../models/dto/response/expert-calendar.model';
import * as moment from 'moment';

@Injectable({
    providedIn: 'root'
})
export class ExpertCalendarResolver implements Resolve<any> {
    constructor(private expert: ExpertService) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
        Observable<ExpertCalendar> | Promise<any> | any {
        return this.expert.getCalendar(+route.paramMap.get('id'), moment().format('YYYY-MM-DD'));
    }
}
