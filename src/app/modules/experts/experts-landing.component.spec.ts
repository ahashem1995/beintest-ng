import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpertsLandingComponent } from './experts-landing.component';

describe('ExpertsLandingComponent', () => {
  let component: ExpertsLandingComponent;
  let fixture: ComponentFixture<ExpertsLandingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpertsLandingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpertsLandingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
