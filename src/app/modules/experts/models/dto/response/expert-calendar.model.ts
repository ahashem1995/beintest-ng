import {ExpertAppointment} from './expert-appointment.model';
import {WorkingHours} from './working-hours.model';

export class ExpertCalendar {
    appointments: Array<ExpertAppointment>;
    workingHours: WorkingHours;
}
