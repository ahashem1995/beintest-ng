import {WorkingHours} from './working-hours.model';

export class ExpertHeader {
  id: number;
  fullName: string;
  profilePicture: string;
  specialization: string;
}

export class ExpertDetails extends ExpertHeader {
  workingHours: WorkingHours;
  country: string;

  constructor() {
    super();
  }
}
