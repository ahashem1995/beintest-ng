import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ExpertDetailsComponent} from './components/expert-details/expert-details.component';
import {ExpertListingComponent} from './components/expert-listing/expert-listing.component';
import {ExpertsLandingComponent} from './experts-landing.component';
import {ExpertCalendarComponent} from './components/expert-calendar/expert-calendar.component';
import {ExpertCalendarResolver} from './resolver/expert-calendar.resolver';
import {ConfirmationMessageComponent} from './components/confirmation-message/confirmation-message.component';

const routes: Routes = [
    {
        path: '',
        component: ExpertsLandingComponent,
        children: [
            {
                path: '',
                component: ExpertListingComponent,
            },
            {
                path: ':id',
                children: [
                    {
                        path: '',
                        component: ExpertDetailsComponent
                    },
                    {
                        path: 'calendar',
                        component: ExpertCalendarComponent,
                        resolve: {calendar: ExpertCalendarResolver}
                    }
                ]
            },
            {
                path: 'book/success',
                component: ConfirmationMessageComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ExpertsRoutingModule {
}
