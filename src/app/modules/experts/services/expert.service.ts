import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {PaginationRequest} from '../../../shared/models/dto/request/pagination-request.model';
import {Observable} from 'rxjs';
import {PaginationResult} from '../../../shared/models/dto/response/pagination-result.model';
import {ExpertDetails, ExpertHeader} from '../models/dto/response/expert.model';
import {API} from '../../../config/api';
import {ServerResponse} from '../../../shared/models/dto/server-response.model';
import {extractData} from '../../../shared/operators/extract-data.operator';
import {ExpertCalendar} from '../models/dto/response/expert-calendar.model';
import {ExpertAppointment} from '../models/dto/response/expert-appointment.model';
import {UserService} from '../../../shared/services/user.service';

@Injectable({
    providedIn: 'root'
})
export class ExpertService {

    constructor(private http: HttpClient, private user: UserService) {
    }

    public paginate(request: PaginationRequest): Observable<PaginationResult<ExpertHeader>> {
        const url = `${API.experts}?${request.queryParams()}`;
        return this.http.get<PaginationResult<ExpertHeader>>(url);
    }

    get(id: number): Observable<ExpertDetails> {
        const url = `${API.experts}/${id}`;
        return this.http.get<ServerResponse<ExpertDetails>>(url)
            .pipe(extractData);
    }

    getCalendar(id: number, day: string): Observable<ExpertCalendar> {
        const tz = this.user.user.timezone; // moment.tz.guess();
        const url = `${API.experts}/${id}/calendar?day=${day}` + (tz ? `&tz=${tz}` : '');
        return this.http.get<ServerResponse<ExpertCalendar>>(url)
            .pipe(extractData);
    }

    addAppointment(id: number, requestBody: any): Observable<ExpertAppointment> {
        const tz = this.user.user.timezone; // moment.tz.guess();
        const url = `${API.experts}/${id}/appointment` + (tz ? `?tz=${tz}` : '');
        return this.http.post<ServerResponse<ExpertAppointment>>(url, requestBody)
            .pipe(extractData);
    }
}
