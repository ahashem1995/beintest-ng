import {Component, Injector, OnInit} from '@angular/core';
import {BaseAppComponent} from '../../shared/models/base/base-app.component';

@Component({
  selector: 'app-experts-landing',
  templateUrl: './experts-landing.component.html',
  styleUrls: ['./experts-landing.component.scss']
})
export class ExpertsLandingComponent extends BaseAppComponent implements OnInit {

  constructor(injector: Injector) {
      super(injector);
  }

  ngOnInit() {
  }

}
