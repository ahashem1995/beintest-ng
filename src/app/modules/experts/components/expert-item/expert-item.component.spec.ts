import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpertItemComponent } from './expert-item.component';

describe('ExpertItemComponent', () => {
  let component: ExpertItemComponent;
  let fixture: ComponentFixture<ExpertItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpertItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpertItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
