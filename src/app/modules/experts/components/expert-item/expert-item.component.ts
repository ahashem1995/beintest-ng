import {Component, Injector, Input, OnInit} from '@angular/core';
import {ExpertHeader} from '../../models/dto/response/expert.model';
import {BaseAppComponent} from '../../../../shared/models/base/base-app.component';

@Component({
  selector: 'app-expert-item',
  templateUrl: './expert-item.component.html',
  styleUrls: ['./expert-item.component.scss']
})
export class ExpertItemComponent extends BaseAppComponent implements OnInit {

  @Input() dataSource: ExpertHeader;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
  }

}
