import {Component, Injector, OnInit} from '@angular/core';
import {BaseAppComponent} from '../../../../shared/models/base/base-app.component';

@Component({
    selector: 'app-confirmation-message',
    templateUrl: './confirmation-message.component.html',
    styleUrls: ['./confirmation-message.component.scss']
})
export class ConfirmationMessageComponent extends BaseAppComponent implements OnInit {

    from: string;

    to: string;

    date: string;

    constructor(injector: Injector) {
        super(injector);
    }

    ngOnInit() {
        this.date = this.route.snapshot.queryParamMap.get('date');
        this.from = this.route.snapshot.queryParamMap.get('from');
        this.to = this.route.snapshot.queryParamMap.get('to');
    }

}
