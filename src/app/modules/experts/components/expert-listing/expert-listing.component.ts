import {Component, Injector, OnInit} from '@angular/core';
import {BaseAppComponent} from '../../../../shared/models/base/base-app.component';
import {PaginationRequest} from '../../../../shared/models/dto/request/pagination-request.model';
import {finalize} from 'rxjs/operators';
import {ExpertHeader} from '../../models/dto/response/expert.model';
import {PaginationResult} from '../../../../shared/models/dto/response/pagination-result.model';
import {BaseUIState} from '../../../../shared/models/base/base-ui-state.model';

@Component({
  selector: 'app-expert-listing',
  templateUrl: './expert-listing.component.html',
  styleUrls: ['./expert-listing.component.scss']
})
export class ExpertListingComponent extends BaseAppComponent implements OnInit {

  page = 1;

  result: PaginationResult<ExpertHeader> = new PaginationResult<ExpertHeader>([]);

  constructor(injector: Injector) {
    super(injector);
    this.state = new BaseUIState();
  }

  ngOnInit() {
    this.getData();
  }

  private getData() {
    this.state.setIsLoading();
    this.expert.paginate(new PaginationRequest(this.page))
      .pipe(finalize(() => this.state.setIsLoading(false)))
      .subscribe(response => this.result = response);
  }
}
