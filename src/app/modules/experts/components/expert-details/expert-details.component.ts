import {Component, Injector, OnInit} from '@angular/core';
import {BaseAppComponent} from '../../../../shared/models/base/base-app.component';
import {BaseUIState} from '../../../../shared/models/base/base-ui-state.model';
import {finalize} from 'rxjs/operators';
import {ExpertDetails} from '../../models/dto/response/expert.model';

@Component({
  selector: 'app-expert-details',
  templateUrl: './expert-details.component.html',
  styleUrls: ['./expert-details.component.scss']
})
export class ExpertDetailsComponent extends BaseAppComponent implements OnInit {

  dataSource: ExpertDetails;

  constructor(injector: Injector) {
    super(injector);
    this.state = new BaseUIState();
  }

  ngOnInit() {
    this.getData();
  }

  private getData(): void {
    this.state.setIsLoading(true);
    const id: number = +this.route.snapshot.paramMap.get('id');
    this.expert.get(id)
      .pipe(finalize(() => this.state.setIsLoading(false)))
      .subscribe(response => this.dataSource = response);
  }
}
