import {Component, Injector, OnInit, ViewChild} from '@angular/core';
import {BaseAppComponent} from '../../../../shared/models/base/base-app.component';
import {BeInFormGroup, ControlConfig} from '../../../../shared/services/be-in-form-builder.service';
import {map} from 'rxjs/operators';
import {ExpertCalendar} from '../../models/dto/response/expert-calendar.model';
import {MatCalendar} from '@angular/material';
import {ExpertAppointment} from '../../models/dto/response/expert-appointment.model';
import * as moment from 'moment';

interface TimeSlot {
    label: string;
    value: { from: string, to: string };
}

const FORMATS = {
    DATE: 'YYYY-MM-DD',
    TIME12: 'hh:mm A',
    TIME24: 'HH:mm:ss',
    DATE_TIME: 'YYYY-MM-DD HH:mm:ss'
};

@Component({
    selector: 'app-expert-calendar',
    templateUrl: './expert-calendar.component.html',
    styleUrls: ['./expert-calendar.component.scss']
})
export class ExpertCalendarComponent extends BaseAppComponent implements OnInit {

    /**
     * Form values container
     */
    form: BeInFormGroup;

    /**
     * Expert calendar of appointments
     */
    calendar: ExpertCalendar;

    /**
     * Time slot select items
     */
    timeSlots: Array<TimeSlot> = [];

    /**
     * Current date for minimum validation
     */
    now: Date = new Date();

    /**
     * User browser timezone
     */
    timezoneOffset = -new Date().getTimezoneOffset() / 60;

    /**
     * Calendar component for events listening
     */
    @ViewChild('calendarElement', {static: true}) calendarElement: MatCalendar<any>;

    constructor(injector: Injector) {
        super(injector);
    }

    static buildSlot(previousStep: moment.Moment, nextStep: moment.Moment): TimeSlot {
        return {
            value: {
                from: previousStep.format(FORMATS.DATE_TIME),
                to: nextStep.format(FORMATS.DATE_TIME),
            },
            label: `${previousStep.format(FORMATS.TIME12)} - ${nextStep.format(FORMATS.TIME12)}`
        };
    }

    ngOnInit() {
        this.route.data
            .pipe(map(data => data.calendar))
            .subscribe((calendar: ExpertCalendar) => this.calendar = calendar);
        this.bindForm();
        this.updateTimeSlots();
        this.subscribeChanges();
    }

    /**
     * Listen to duration change and calendar date change to fetch appointments from server and update time slots
     */
    private subscribeChanges() {
        this.form.get('duration').valueChanges
            .subscribe((value: number) => {
                const timeSlotFormCtrl = this.form.get('timeSlot');
                value ? timeSlotFormCtrl.enable() : timeSlotFormCtrl.disable();
                this.updateTimeSlots();
            });
        this.calendarElement.selectedChange
            .subscribe((value: (Date)) => {
                this.form.get('date').setValue(value);
                this.getCalendar(this.moment(value).format(FORMATS.DATE));
            });
    }

    /**
     * Get data from server
     * @param day to fetch data for
     */
    private getCalendar(day: string): void {
        const id = +this.route.snapshot.paramMap.get('id');
        this.expert.getCalendar(id, day)
            .subscribe(calendar => {
                this.calendar = calendar;
                this.updateTimeSlots();
            });
    }

    /**
     * Update time slots select items handle
     */
    private updateTimeSlots(): void {
        this.timeSlots = [];
        const {appointments, workingHours} = this.calendar;

        if (this.form.get('duration').valid) {
            const baseDate = this.moment(this.form.get('date').value).format(FORMATS.DATE);
            const openTime = `${baseDate} ${workingHours.openTime}`;
            const closeTime = `${baseDate} ${workingHours.closeTime}`;

            if (workingHours.openTime < workingHours.closeTime) {
                // Simple handling, one domain
                this.timeSlots.push(...this.buildTimeSlots(openTime, closeTime));
            } else {
                // Two time domains [[openTime, 24:00:00], [00:00:00, closeTime]]
                const startOfDay = `${baseDate} 00:00:00`;
                const endOfDay = `${baseDate} 23:59:59`;

                this.timeSlots.push(...this.buildTimeSlots(openTime, endOfDay));
                this.timeSlots.push(...this.buildTimeSlots(startOfDay, closeTime));
            }
        }
    }

    /**
     * Submit a new appointment booking
     */
    submit(): void {
        const requestBody = {
            from: this.form.value.timeSlot.from,
            to: this.form.value.timeSlot.to,
            duration: this.form.value.duration,
            userId: this.user.user.id
        };
        this.expert.addAppointment(+this.route.snapshot.paramMap.get('id'), requestBody)
            .subscribe((response) => {
                this.router.navigate(['/experts/book/success'], {
                    queryParams: {
                        from: this.moment(response.from).format(FORMATS.TIME12),
                        to: this.moment(response.to).format(FORMATS.TIME12),
                        date: this.moment(response.from).format(FORMATS.DATE)
                    }
                }).then();
            });
    }

    /**
     * Bind form initial values
     */
    private bindForm() {
        this.form = this.fb.bindForm({
            date: new ControlConfig(new Date(), ['required']),
            duration: new ControlConfig('', ['required']),
            timeSlot: new ControlConfig('', ['required'], true),
        });
    }

    /**
     * Helper method which build time slots out of time range
     * @param from string date time
     * @param to string date time
     */
    private buildTimeSlots(from: string, to: string): Array<TimeSlot> {
        const slotsArray: Array<TimeSlot> = [];
        // Filter appointments between arguments range
        const domain: Array<ExpertAppointment> = this.calendar.appointments
            .filter(a => this.moment(a.from).isSameOrAfter(this.moment(from)) &&
                this.moment(a.to).isSameOrBefore(to));
        const duration = this.form.get('duration').value;
        let previousAppointment: moment.Moment;
        let nextAppointment: moment.Moment;
        let stepsBetween: number;
        let previousStep;
        let nextStep;

        for (let i = 0; i <= domain.length; i++) {
            previousStep = null;
            nextStep = null;
            previousAppointment = this.moment(i === 0 ? from : domain[i - 1].to);
            nextAppointment = this.moment(i === domain.length ? to : domain[i].from);
            stepsBetween = Math.floor(this.moment.duration(nextAppointment.diff(previousAppointment)).asMinutes() / duration);

            previousStep = previousAppointment.clone();
            nextStep = previousAppointment.clone();
            for (let j = 0; j < stepsBetween; j++) {
                previousStep = j === 0 ? previousAppointment.clone() : nextStep.clone();
                nextStep = previousStep.clone().add(duration, 'm');
                slotsArray.push(ExpertCalendarComponent.buildSlot(previousStep, nextStep));
            }
        }

        return slotsArray;
    }
}
