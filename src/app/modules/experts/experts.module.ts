import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ExpertsRoutingModule} from './experts-routing.module';
import {ExpertDetailsComponent} from './components/expert-details/expert-details.component';
import {ExpertListingComponent} from './components/expert-listing/expert-listing.component';
import {ExpertItemComponent} from './components/expert-item/expert-item.component';
import {
    MatButtonModule,
    MatCardModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatGridListModule, MatInputModule, MatNativeDateModule,
    MatProgressSpinnerModule,
    MatSelectModule
} from '@angular/material';
import {ExpertsLandingComponent} from './experts-landing.component';
import {ExpertCalendarComponent} from './components/expert-calendar/expert-calendar.component';
import {InputSelectModule} from '../../shared/components/input-select/input-select.module';
import {BeInFormModule} from '../../shared/components/be-in-form/be-in-form.module';
import {ReactiveFormsModule} from '@angular/forms';
import { ConfirmationMessageComponent } from './components/confirmation-message/confirmation-message.component';


@NgModule({
    declarations: [
        ExpertsLandingComponent,
        ExpertItemComponent,
        ExpertDetailsComponent,
        ExpertListingComponent,
        ExpertCalendarComponent,
        ConfirmationMessageComponent
    ],
    imports: [
        CommonModule,
        ExpertsRoutingModule,
        MatGridListModule,
        MatCardModule,
        MatProgressSpinnerModule,
        InputSelectModule,
        BeInFormModule,
        MatFormFieldModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatInputModule,
        ReactiveFormsModule,
        MatButtonModule
    ],
    exports: [
        ExpertItemComponent
    ],
    providers: [
        MatDatepickerModule
    ]
})
export class ExpertsModule {
}
