import {environment} from '../../environments/environment';

export const API = {
    experts: environment.baseUrl + 'experts',
    users: environment.baseUrl + 'users'
};
