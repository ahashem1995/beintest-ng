// Abstract
export const ADD = 'add';
export const EDIT = 'edit';
export const PAGE_NOT_FOUND = 'page-not-found';

// Experts module
export const EXPERTS = 'experts';

// Users
export const USERS = 'users';
